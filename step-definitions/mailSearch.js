const { client } = require('nightwatch-api');
const { Given, Then, When } = require('cucumber');

Given(/^I open Mail's search page$/, () => {
  return client.url('https://mail.ru/search').waitForElementVisible('body', 1000);
});

Then(/^the title contains is "(.*?)"$/, text => {
  return client.assert.titleContains(text);
});

When(/^I search "(.*?)" in the search form$/, searchSring => {
  return client.frame(0).assert.visible('.mini-suggest__input').setValue('.mini-suggest__input', searchSring);
});

When(/^press button "Найти"$/, () => {
  return client.assert.visible('.mini-suggest__button').click('.mini-suggest__button');
});

Then(/^"(.*?)"th search result from "(.*?)"$/, (numberOfResult, searchUrl )  => {
  --numberOfResult;
  return client.waitForElementVisible(`li[data-cid="${numberOfResult}"]`, 5000)
      .assert.attributeContains(`li[data-cid="${numberOfResult}"] .Link_theme_outer`, 'href', searchUrl)
      .assert.containsText(`li[data-cid="${numberOfResult}"] .OrganicTitleContentSpan b:first-child`, 'Великая')
      .assert.containsText(`li[data-cid="${numberOfResult}"] .OrganicTitleContentSpan b:nth-child(2)`, 'Китайская')
      .assert.containsText(`li[data-cid="${numberOfResult}"] .OrganicTitleContentSpan b:nth-child(3)`, 'стена')

});

