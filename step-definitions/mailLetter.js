const { client } = require('nightwatch-api');
const { Given, Then, When } = require('cucumber');
const {cli} = require("nightwatch");


Given(/^I open "(.*?)" page$/, (urlName) => {
  client.url('https://mail.ru/').waitForElementVisible('body', 1000);
  client.click('.balloon__close');
  return client.assert.titleContains(urlName);
});

When(/^I open mail box with login "(.*?)" and password "(.*?)"$/, (logIn, pasw) => {
   client.click('button.ph-login');
   client.waitForElementVisible('iFrame.ag-popup__frame__layout__iframe', 10000);

  client.elements("css selector", "iFrame", function(link_array) {
    for (var x = 0; x < link_array.value.length; x++){
        let y = x;
        client.elementIdAttribute(link_array.value[x].ELEMENT, "src", function(links) {
        if (!links.value.error){
            if (links.value.includes('account.mail.ru/login')){
                // console.log(links.value, y);

                client.frame(y).waitForElementVisible('input[name="username"]', 10000)
                    .setValue('input[name="username"]', logIn)
                    .click('button[type="submit"]');

                return;
            }
        }
        });
    }
  });

   return client.waitForElementVisible('input[name="password"]', 10000)
       .setValue('input[name="password"]', pasw)
       .click('button[type="submit"]');
});

Then(/^I see "(\d)" letters$/, (l) => {
    client.pause(10000);
    client.elements('xpath', '//a[contains(@href, "/inbox/1")]', function (result) {
    client.assert.equal(result.value.length, l);
    });
    return client.pause(5000);
});

When(/^letters have been read$/, ()=> {
    return client.waitForElementVisible('body', 5000).verify.visible('span.badge__text');
});

Then(/^mark them unread$/, ()=> {
    return client.sendKeys("body", [client.Keys.CONTROL, "a"]).pause(1000).sendKeys('body', 'u')
        .verify.containsText('span.badge__text', '5').pause(5000);
});

Then(/^I open them in new browser tab$/, ()=> {
    return client.elements("xpath", "//a[contains(@href, '/inbox/1')]", function(link_array_a) {
     client.windowHandle((parentHandler) => {
        for (let i = 0; i < link_array_a.value.length; i++){
            client.elementIdAttribute(link_array_a.value[i].ELEMENT, "href", function(links_a) {
                    client.openNewWindow(function(childHandler) {
                    client.switchWindow(childHandler.value.handle).url(links_a.value)
                          .waitForElementVisible('body', 5000).switchWindow(parentHandler.value);
                    });
            });
        }
     });
  }).pause(5000);
});

Then (/^I see in mail box all letters are read$/, ()=>{
    return client.waitForElementVisible('body', 5000)
        .isVisible('span.badge__text', res => {
                if(res.status != 0) {
                    console.log('~All letters are read')
                }
                else {
                    console.log('~NOT all letters are read')
                }
                })
        .pause(3000);
});