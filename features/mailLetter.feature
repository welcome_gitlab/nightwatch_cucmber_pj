# Created by Stanislav at 07.06.2023

@mailRead
Feature: Mail Read
  # Enter feature description here

  @read
  Scenario: Reading letters
    Given I open "Mail.ru" page
    When I open mail box with login "stanislavd1951@mail.ru" and password "$uYsOtguOI51"
    Then I see "5" letters
    When letters have been read
    Then mark them unread
    And  I open them in new browser tab
    And I see in mail box all letters are read
