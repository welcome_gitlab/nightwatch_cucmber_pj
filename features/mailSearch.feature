# Created by Stanislav at 06.06.2023

@mailSearch
Feature: Mail Search
  # Enter feature description here

  @search
  Scenario: Searching Mail
    Given I open Mail's search page
    Then the title contains is "Mail"
    When I search "великая китайская стена" in the search form
    And press button "Найти"
    Then "2"th search result from "wikiway.com"

